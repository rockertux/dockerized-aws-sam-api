#!/bin/sh

env_file=$1.env

while read line; do
  export $line
done < $env_file

sed "s/\${DATABASE_URL}/postgres:\/\/$POSTGRES_USER:$POSTGRES_PASSWORD@db:5432\/$POSTGRES_DB/" api/template.base.yaml > api/template.yaml
sed "s/\${ENV_FILE}/$env_file/" docker-compose.base.yml > docker-compose.yml

if [ "$1" != "test" ]
then
  sed -i "/TEST-ONLY/,/END-TEST-ONLY/d" docker-compose.yml
else
  sed -i "/TEST-ONLY/d" docker-compose.yml
fi

docker-compose up
