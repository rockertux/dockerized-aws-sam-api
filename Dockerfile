FROM python:alpine

LABEL maintainer="Arthur Miranda <tux@tuxmiranda.com>"

RUN mkdir /app
WORKDIR /app
RUN pip install aws-sam-cli

CMD ["sh"]
