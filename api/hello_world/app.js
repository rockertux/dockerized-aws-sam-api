const axios = require('axios')
const url = 'http://checkip.amazonaws.com/';
const aws = require('aws-sdk');

exports.lambda_handler = async (event, context, callback) => {
  let response;

  try {
    const ret = await axios(url);
    response = {
      'statusCode': 200,
      'body': JSON.stringify({
        message: 'hello world',
        location: ret.data.trim(),
        event,
        context,
        env: process.env,
      })
    }
  }
  catch (err) {
    console.log(err);
    callback(err, null);
  }

  callback(null, response)
};
